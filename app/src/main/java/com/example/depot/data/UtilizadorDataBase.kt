package com.example.depot.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.depot.modelo_de_dados.Utilizador


@Database(entities = [Utilizador::class], version = 1, exportSchema = false)
abstract class UtilizadorDataBase: RoomDatabase() {

    // Função que retorna nosso objeto de acesso de data "Dao" do utilizador
    abstract fun utilizadorDao(): UtilizadorDao


    // Tudo que colocarmos nesse objeto será visivel e utilizavel para outras classes do projeto
    companion object{

        // Singleton class significa que nossa base de dados do utilizadores só terá uma unica instancia
        // da sua classe
        // @Volatile = Escritas nesse campos são vistas imediatamente por outras threads
        @Volatile
        private var INSTANCE: UtilizadorDataBase? = null


        fun getDatabase(context: Context):UtilizadorDataBase{

            // Checamos se a instancia não é vazia, se nn for a instancia ja existe e retornamos a mesma
            val tempInstance = INSTANCE
            if ( tempInstance != null) {
                return tempInstance
            }

            // Criar uma nova isntancia se a mesma for vazia
            // synchronized = tudo nesse bloco vai ser protegido de execuções concorrentes,
            // estamos criando uma instancia da nossa base de dados ROOM
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    UtilizadorDataBase::class.java,
                    "Utilizador_tabela"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }



}