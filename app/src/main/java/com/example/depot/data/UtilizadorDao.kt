package com.example.depot.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.depot.modelo_de_dados.Servico
import com.example.depot.modelo_de_dados.Utilizador
import com.example.depot.modelo_de_dados.Utilizador_servico


@Dao
interface UtilizadorDao {
    // IGNORE = se tivermos o mesmo utiizador na tabela, iremos ignorar isso
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun adicionarUtilizador(utilizador: Utilizador)

    @Update
    suspend fun atualizarUtilizador(utilizador: Utilizador)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun inserirServico(servico: Servico)



    @Query("SELECT * FROM utilizador_tabela ORDER BY ID_utilizador ASC")
    fun lerTodaData(): LiveData<List<Utilizador>>


    // Retorna uma lista com os servicos que são praticados pelos usuarios
    @Transaction
    @Query("SELECT * FROM utilizador_tabela WHERE ID_utilizador = :utilizador")
    suspend fun getUtilizadoresComServicos(utilizador: String): List<Utilizador_servico>


}