package com.example.depot.repository

import androidx.lifecycle.LiveData
import com.example.depot.data.UtilizadorDao
import com.example.depot.modelo_de_dados.Utilizador

// Classe abstrata que acessa multiplas fontes de data
// Dao necessário como parametro

class UtilizadorRepository(private val utilizadorDao: UtilizadorDao) {

    // Lista dos utilizadores
    val lerTodaData: LiveData<List<Utilizador>> = utilizadorDao.lerTodaData()

    suspend fun adicionarUtilizador(utilizador: Utilizador){
        utilizadorDao.adicionarUtilizador(utilizador)
    }

    suspend fun atualizarUtilizador(utilizador: Utilizador){
        utilizadorDao.atualizarUtilizador(utilizador)
    }
}