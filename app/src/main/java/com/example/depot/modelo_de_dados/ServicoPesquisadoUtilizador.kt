package com.example.depot.modelo_de_dados

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

// Nos ajuda com query aonde queremos uma lista dos serviços que foram pesquisados por "x" utilizador
data class ServicoPesquisadoUtilizador (
    @Embedded
    val utilizador: Utilizador,
    @Relation(
        parentColumn = "ID_utilizador",
        entityColumn = "ID_servico",
        associateBy = Junction(Pesquisa::class)
    )
    val servicos: List<Servico>
)
