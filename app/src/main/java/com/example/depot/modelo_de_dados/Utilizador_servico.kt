package com.example.depot.modelo_de_dados

import androidx.room.Embedded
import androidx.room.Relation

data class Utilizador_servico(

    // Instanciamos a classe de utilizador para podermos referir qual seria a coluna
    // que teria os valores do id de serviço (Qual sua foreign key)
    @Embedded
    val utilizador: Utilizador,
    @Relation(
        parentColumn = "ID_utilizador",
        entityColumn = "utilizador"
    )
    val servicos: List<Servico>

)