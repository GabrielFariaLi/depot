package com.example.depot.modelo_de_dados

import androidx.room.Entity
import java.util.*

@Entity(primaryKeys = ["utilizador", "servico"])
data class Pesquisa (
    val utilizador: Int,
    val servico: Int,
    val data: Date,
)
