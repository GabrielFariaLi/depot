package com.example.depot.modelo_de_dados

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

// Nos ajuda com query aonde queremos uma lista dos utilizadores que publicam "x" serviço
data class UtilizadorPublicaServico (
    @Embedded
    val servico: Servico,
    @Relation(
        parentColumn = "ID_servico",
        entityColumn = "ID_utilizador",
        associateBy = Junction(Pesquisa::class)
    )
    val utilizadores: List<Utilizador>
)
