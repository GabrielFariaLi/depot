package com.example.depot.modelo_de_dados

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "Servico_tabela")
data class Servico(
    @PrimaryKey(autoGenerate = true)
    val ID_servico: Int,
    val utilizador: Int,
    val preco: Double,
    val titulo: String,
    val localizacao: String,
    val dominio: String?,
    val descricao: String?,
    val nivel_profissional: String?,
    val disponibilidade: String?,
    val imagem: String?,
    val em_destaque: Boolean,
): Parcelable