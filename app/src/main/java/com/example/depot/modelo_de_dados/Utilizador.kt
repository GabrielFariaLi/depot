package com.example.depot.modelo_de_dados

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "Utilizador_tabela")
data class Utilizador(
    @PrimaryKey(autoGenerate = true)
    val ID_utilizador: Int,
    val primeiro_nome: String,
    val ultimo_nome: String,
    val avaliacao: Double,
    val competencia: String,
    val cidade: String?,
    val pais: String?,
    val contacto: String?,
    val NIF: Int?,
    val servico: Int,
): Parcelable