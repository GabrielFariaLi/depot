package com.example.depot.fragments.adicionar

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_adicionar.*
import kotlinx.android.synthetic.main.fragment_adicionar.view.*
import com.example.depot.R
import com.example.depot.modelo_de_dados.Utilizador
import com.example.depot.view_model.UtilizadorViewModel


class AdicionarFragment : Fragment() {

    private lateinit var mUtilizadorViewModel: UtilizadorViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_adicionar, container, false)

        mUtilizadorViewModel = ViewModelProvider(this).get(UtilizadorViewModel::class.java)

        view.adicionarBtn.setOnClickListener{
            inserirDataParaDataBase()
        }

        return view
    }

    private fun inserirDataParaDataBase() {
        val primeiro_nome = adicionarPrimeiroNome_et.text.toString()
        val ultimo_nome = adicionarUltimoNome_et.text.toString()
        val competencia = adicionarCompetencia_et.text.toString()

        if(checarInput(primeiro_nome, ultimo_nome, competencia)){
            // Cria o objeto de utilizador
            val utilizador = Utilizador(0,primeiro_nome,ultimo_nome,0.0, competencia, null,null,null,null/*Integer.parseInt(NIF.toString())*/)
            // Adicionar Data para a Data Base
            mUtilizadorViewModel.adicionarUtilizador(utilizador)
            Toast.makeText(requireContext(),"Utilizador Adicionado com sucesso!", Toast.LENGTH_LONG).show()
            // Navegar de volta
            findNavController().navigate(R.id.action_adicionarFragment_to_listaFragment)
        }else {
            Toast.makeText(requireContext(),"Porfavor preencha todos os campos!", Toast.LENGTH_LONG).show()
        }
    }

    private fun checarInput(primeiro_nome: String, ultimo_nome:  String, competencia: String): Boolean{
        return !(TextUtils.isEmpty(primeiro_nome) && TextUtils.isEmpty(ultimo_nome) && TextUtils.isEmpty(competencia))
    }
}