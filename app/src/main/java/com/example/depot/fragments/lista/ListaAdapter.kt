package com.example.depot.fragments.lista

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.linha_customizada.view.*
import com.example.depot.R
import com.example.depot.modelo_de_dados.Utilizador
import com.example.depot.fragments.lista.ListaFragmentDirections

class ListaAdapter: RecyclerView.Adapter<ListaAdapter.MinhaViewHolder>() {

    // Tem uma lista tipo nosso modelo de classes (utilizadores)
    private var lista_utilizador = emptyList<Utilizador>()

    class MinhaViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    }
    // Vamos inflar essa view holder criada
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MinhaViewHolder {

        return MinhaViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.linha_customizada, parent, false))
    }

    override fun getItemCount(): Int {

        return lista_utilizador.size
    }

    override fun onBindViewHolder(guardar_linha_customizada: MinhaViewHolder, posicao: Int) {
        val item_atual = lista_utilizador[posicao]

        guardar_linha_customizada.itemView.id_utilizador_txt.text = item_atual.ID_utilizador.toString()
        // Encontrar o id
        guardar_linha_customizada.itemView.primeiroNome_txt.text = item_atual.primeiro_nome
        guardar_linha_customizada.itemView.ultimoNome_txt.text = item_atual.ultimo_nome
        guardar_linha_customizada.itemView.competencia_txt.text = item_atual.competencia
        guardar_linha_customizada.itemView.linha_Layout.setOnClickListener{
            // classe automaticamente gerada pela nossa listaFragment, podemos passar nossos obje
            // tos do utilizador para o atualizarFragment
            val acao = ListaFragmentDirections.actionListaFragmentToAtualizarFragment(item_atual)
            // Sempre que um utilizador sleecionar um item na recycler view, nos vamos passar esse
            // utilizador atual para o nosso atualizarFragment (Para sabermos qual iremos editar)
            guardar_linha_customizada.itemView.findNavController().navigate(acao)
        }
    }

    fun setData(utilizador: List<Utilizador>) {
        this.lista_utilizador = utilizador
        notifyDataSetChanged()
    }


}