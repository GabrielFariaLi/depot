package com.example.depot.fragments.atualizar

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.depot.R
import com.example.depot.modelo_de_dados.Utilizador
import com.example.depot.view_model.UtilizadorViewModel
import kotlinx.android.synthetic.main.fragment_atualizar.*
import kotlinx.android.synthetic.main.fragment_atualizar.view.*
import kotlinx.android.synthetic.main.linha_customizada.*
import com.example.depot.fragments.atualizar.AtualizarFragmentArgs


class AtualizarFragment : Fragment() {

    private val args by navArgs<AtualizarFragmentArgs>()

    private lateinit var mUtilizadorViewModel: UtilizadorViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_atualizar, container, false)

        //iniicar view model
        mUtilizadorViewModel = ViewModelProvider(this).get(UtilizadorViewModel::class.java)

        // Args passados no navigator para pergamos as informações atuais
        view.atualizarPrimeiroNome_et.setText(args.utilizadorAtual.primeiro_nome)
        view.atualizarUltimoNome_et.setText(args.utilizadorAtual.ultimo_nome)
        view.atualizarCompetencia_et.setText(args.utilizadorAtual.competencia)

        view.atualizarBtn.setOnClickListener{
            atualizarItem()
        }

        return view
    }

    private fun atualizarItem(){
        val primeiro_nome = atualizarPrimeiroNome_et.text.toString()
        val ultimo_nome = atualizarUltimoNome_et.text.toString()
        val competencia = atualizarCompetencia_et.text.toString()

        if(checarInput(primeiro_nome, ultimo_nome, competencia)){
            // Criar objeto de utilizador atualizado
            val utilizador_atualizado = Utilizador(args.utilizadorAtual.ID_utilizador,primeiro_nome,ultimo_nome,0.0,competencia,null,null,null,null)
            // atualizar o utilizador atual
            mUtilizadorViewModel.atualizarUtilizador(utilizador_atualizado)
            Toast.makeText(requireContext(), "Atualização realizada com sucesso!", Toast.LENGTH_SHORT).show()
            // Navegar de volta
            findNavController().navigate(R.id.action_atualizarFragment_to_listaFragment)
        }else{
            Toast.makeText(requireContext(), "Porfavor preencha todos os campos!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun checarInput(primeiro_nome: String, ultimo_nome:  String, competencia: String): Boolean{
        return !(TextUtils.isEmpty(primeiro_nome) && TextUtils.isEmpty(ultimo_nome) && TextUtils.isEmpty(competencia))
    }
}