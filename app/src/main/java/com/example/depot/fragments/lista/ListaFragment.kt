package com.example.depot.fragments.lista

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_lista.view.*
import com.example.depot.R
import com.example.depot.view_model.UtilizadorViewModel


class ListaFragment : Fragment() {

    // Variavel para nosso utilizadorviewmodel
    private lateinit var mUtilizadorViewModel: UtilizadorViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_lista, container, false)

        // RecyclerView
        val adapter = ListaAdapter()
        val recyclerView = view.recyclerview
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        // UtilizadorViewModel
        mUtilizadorViewModel = ViewModelProvider(this).get(UtilizadorViewModel::class.java)
        // Sempre que uma data for atualizada modificada ou apagada nos queremos saber esse novo valor
        mUtilizadorViewModel.lerTodaData.observe(viewLifecycleOwner, Observer { utilizador ->
            adapter.setData(utilizador)
        })

        view.floatingActionButton.setOnClickListener{
            findNavController().navigate(R.id.action_listaFragment_to_adicionarFragment)
        }

        return view
    }


}