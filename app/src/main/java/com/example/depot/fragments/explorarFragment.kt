package com.example.depot

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.depot.R
import kotlinx.android.synthetic.main.fragment_explorar.view.*


class explorarFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_explorar, container, false)
        //view.editTextPesquisar.findNavController().navigate(R.id.action_explorarFragment_to_pesquisarFragment)
        view.listaUtilizadores_btn.setOnClickListener{
            view.listaUtilizadores_btn.findNavController().navigate(R.id.action_explorarFragment_to_listaFragment)
        }
        return view
    }
}