package com.example.depot.view_model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.depot.data.UtilizadorDataBase
import com.example.depot.modelo_de_dados.Utilizador
import com.example.depot.repository.UtilizadorRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


// Provir data para a interface Grafíca e sobreviver a mudança de configurações
// Serve como meio de comunicação entre o Repository e a interface grafíca
class UtilizadorViewModel(application: Application): AndroidViewModel(application) {

    // Referenciar a lista dos utilizadores e o repository dos utilizadores
    val lerTodaData: LiveData<List<Utilizador>>
    val repository: UtilizadorRepository

    /* METODOS DE ESCRITA E LEITURA E EDIÇÃO E APAGAR DA BASE DE DADOS */

    // Sempre a RRIMEIRA EXECUÇÃO quando o view model do utilizador for chamado
    init {
        val utilizadorDao = UtilizadorDataBase.getDatabase(application).utilizadorDao()
        repository = UtilizadorRepository(utilizadorDao)
        lerTodaData = repository.lerTodaData
    }

    // Metodo de adicionar utilizador
    fun adicionarUtilizador(utilizador: Utilizador){
        // co-rotina
        //dispatchers.io = rodar em uma thread de plano de fundo
        viewModelScope.launch(Dispatchers.IO) {
            // Chama a função do repository dos utilizadores
            repository.adicionarUtilizador(utilizador)
        }
    }

    // Metodo de atualizar utilizador
    fun atualizarUtilizador(utilizador: Utilizador){
        viewModelScope.launch(Dispatchers.IO) {
            repository.atualizarUtilizador(utilizador)
        }
    }
}